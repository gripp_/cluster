const express = require('express');
const fs = require('fs');

const { getClusterManager, getIdentManager } = require(
    '@curvebreakers/cluster');
const clusterManager = getClusterManager();
const identManager = getIdentManager();

// RPC calls.
let allIdents = function () {
  return { 'idents': identManager.all(), 'success': true, };
};

let createIdent = function (name, passphrase) {
  identManager.add(identManager.create(name, passphrase));
  return { 'success': true, };
};

let setIdent = function (name, passphrase) {
  identManager.login(name, passphrase);
};


// Start the server.
app = express();

app.use(express.json());

app.use(express.static('cluster-fe'));

app.get('/', function(req, resp){
  resp.writeHead(200, { 'Content-Type': 'text/html' });
  fs.readFile('./cluster-fe/index.html', (error, content) => {
    resp.type('html');
    resp.send(content);
  });
});

app.post('/rpc/*', function(req, resp) {
  let method = req.url.slice(5);
  resp.type('json');

  let content = {};

  if (method == 'create-ident') {
    content = createIdent(req.body.name, req.body.passphrase);
  } else if (method == 'all-idents') {
    content = allIdents();
  }

  resp.send(JSON.stringify(content));
});

app.listen(8080);
