CONTRIBUTOR AGREEMENT

This is an agreement between glassEyeballs, LLC (MANAGER), as the manager of the contained
 codebase (CLUSTER) and a contributing programmer (CONTRIBUTOR).

By uploading code, configuration files, or data of any kind to the CLUSTER codebase, CONTRIBUTOR
 agrees that such data and the associated rights thereto are assigned to MANAGER. CONTRIBUTOR
  retains a world-wide, non-exclusive, royalty-free and perpetual right to use, copy, modify
  , communicate and make available to the public (including without limitation via the Internet
   and any later developed manner of transmission) and distribute, in each case in an original or
    modified form, contributed data as they wish.

CONTRIBUTOR guarantees:
* CONTRIBUTOR is the author of the uploaded data,
* CONTRIBUTOR is not infringing on any third-party claim to the uploaded data,
* CONTRIBUTOR does not have an employer that can claim ownership of the uploaded data, and
* the uploaded data constitutes original work of the CONTRIBUTOR.

Contribution via the uploading of data as described above constitutes assent to this agreement.
