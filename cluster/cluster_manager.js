const { Client: HyperspaceClient, Server: HyperspaceServer } =
    require('hyperspace');
const { HOST, STORAGE, NAMESPACE } = require('./const');
const Stream = require('stream');
const { generateKeyPairSync } = require('crypto');

const Cluster = require('./cluster');

class ClusterManager {
  constructor() {
    this.server = new HyperspaceServer(
        {
          host: HOST,
          storage: STORAGE,
        });

    this.client = new HyperspaceClient({ host: HOST });
    this.client.network.on('peer-add', this.addPeer);

    this.clusters = [];
    this.peers = [];

    this.corestore = this.client.corestore(NAMESPACE);
  }

  async cleanup() {
    try {
      await this.server.close();
    }
    catch (err) {
      console.log(err);
    }
  }

  addPeer(peer) {
    this.peer.append(peer);
    let clusterKeysStream = Stream.Readable.from(
        this.clusters.map((c) => { return c.key() }));

    let responseStream = Stream.Writeable();
    responseStream._write = (chunk, encoding, next) => {
      let ledger = this.client.corestore(NAMESPACE).get(
          {
            key: key,
            valueEncoding: 'json'
          });
      // TODO sync clusters
      for (let block in ledger.createReadStream()) { }
      this.client.replicate(ledger);
      next();
    };

    identStream.pipe(peer.stream).pipe(responseStream);
  }

  create(name) {
    // TODO verify name.
    let cluster = new Cluster(name, this.client, this.ident.name());
    this.clusters.append(cluster);
    return cluster;
  }

  getCluster(clusterKey) {
    return this.clusters.find((c) => { return c.key() == clusterKey; });
  }

  idents(clusterKey) {
    this.getCluster(clusterKey).idents()
  }

  stamp(stampee, clusterKey) {
    let cluster = this.getCluster(clusterKey);

    if (cluster == undefined) {
      return false;
    }

    return cluster.stamp(this.ident, stampee);
  }

  setIdent(ident) {
    this.ident = ident;
  }

  async start() {
    await this.server.ready();
  }
}

module.exports = ClusterManager
