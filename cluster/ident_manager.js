const { generateKeyPairSync } = require('crypto');

const Ident = require('./ident');

class IdentManager {
  constructor(name, publicKey) {
    this.allIdents = [];
    this.name = name;
    this.publicKey = publicKey;
  }

  add(ident) {
    this.allIdents.push(ident);
  }

  all() {
    return this.allIdents.map(i => i.json());
  }

  create(name, passphrase) {
    // TODO Validate password.
    let timestamp = Date.now();
    let { publicKey, privateKey } = generateKeyPairSync(
        'rsa',
        {
          modulusLength: 4096,
          publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
          },
          privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem',
            cipher: 'aes-256-cbc',
            passphrase: passphrase.concat(timestamp.toString()),
          }
        });
    return new Ident(name, timestamp, publicKey);
  }

  login(name, passphrase) {
    let matches = this.allIdents.filter(
      (i) => {
        return i.name() == name && i.authenticate(passphrase);
      });
  }
}

module.exports = IdentManager;
