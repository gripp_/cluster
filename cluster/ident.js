const crypto = require('crypto');
const random = require('node-random');

class Ident {
  constructor(name, timestamp, publicKey) {
    this.name = name;
    this.publicKey = publicKey;
    this.timestamp = timestamp;
  }

  async authenticate(passphrase) {
    let modifiedPassphrase = passphrase.concat(this.timestamp.toString());
    let secret = await random.strings(
        {},
        (error, data) => {
          if (error) {
            throw error;
          }

          return data[0]
        });

    let sign = crypto.createSign('aes-256-cbc');
    sign.write(secret);
    sign.end();

    let signature = sign.sign(modifiedPassphrase);

    let verify = crypto.createVerify('aes-256-cbc');
    verify.write(secret);
    verify.end();
    return verify.verify(this.publicKey, signature, modifiedPassphrase);
  }

  is(otherIdent) {
    return (
        this.name == otherIdent.name &&
        this.publicKey == otherIdent.publicKey &&
        this.timestamp == otherIdent.timestamp);
  }

  json() {
    return {
      name: this.name,
      publicKey: this.publicKey,
      timestamp: this.timestamp,
    };
  }

  name() {
    return this.name;
  }
}

module.exports = Ident
