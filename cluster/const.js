module.exports = {
  HOST: 'cluster-host',
  LEDGERMARK: 'LEDGERMARK',
  NAMESPACE: 'cluster-store',
  SIGNATURE: 'signature',
  STAMP: 'STAMP',
  STORAGE: './.cluster/data'
};
