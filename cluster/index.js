let ClusterManager = require('./cluster_manager');
let IdentManager = require('./ident_manager');

let CLUSTER_MANAGER = new ClusterManager();
let IDENT_MANAGER = new IdentManager();

exports.getClusterManager = function() {
  return CLUSTER_MANAGER;
}

exports.getIdentManager = function() {
  return IDENT_MANAGER;
}
