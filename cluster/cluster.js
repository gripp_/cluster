const Ident = require('./ident');
const { LEDGERMARK, NAMESPACE, SIGNATURE, STAMP } = require('./const');

class Cluster {
  constructor(name, client, ident) {
    this.client = client;
    this.membership = [ident];
    this.name = name;
    this.timestamp = Date.now();

    this.ledger = this.client.store.namespace(NAMESPACE).get(
        { valueEncoding: 'json' });

    let ledgermark = {
      type: LEDGERMARK,
      mark: this.name,
    };
    this.ledger.append(ledgermark);
    this.stamp(ident, ident);
    this.client.replicate(this.ledger);
  }

  generateMembership() {
    this.membership = [];

    let stampsNecessary = 1;
    let stampsFor = {};

    for (let cert of this.ledger) {
      if (cert.type == LEDGERMARK) {
        continue;
      }

      let stampee = new Ident(
          cert.stampee.name, cert.stampee.timestamp, cert.stampee.publicKey);
      let stamper = new Ident(
          cert.stamper.name, cert.stamper.timestamp, cert.stamper.publicKey);

      if (!$(stampee.name in stampsFor)) {
        stampsFor[stampee.name] = 0;
      }
      stampsFor[stampee.name] += 1;

      if (stampsFor[stampee.name] >= stampsNecessary) {
        // TODO check that this ident isnt already in the list somehow
        this.membership.append(stampee);

        stampsNecessary = Math.floor(
            (.005 * (this.membership.length ^ 2)) +
            (.25 * this.membership.length))
        stampsNecessary = Math.max(1, stampsNecessary);
        stampsNecessary = Math.min(
            this.membership.length - 1, stampsNecessary);
      }
    }
  }

  key() {
    return this.ledger.key;
  }

  members() {
    return this.membership;
  }

  stamp(stamper, stampee) {
    let stamperIsMember = this.members().some(
        (m) => { return m.name == stamper.name; });
    let stampeeIsMember = this.members().some(
        (m) => { return m.name == stampee.name; });

    if (stampeeIsMember || !stamperIsMember) {
      return false;
    }

    for (let block in this.ledger.createReadStream()) {
      if (block.type != STAMP) {
        continue;
      }

      let blockIdent = new Ident(
          this.stampee.name, this.stampee.timestamp, this.stampee.publicKey);

      if (blockIdent.name == stampee.name && !blockIdent.is(stampee)) {
        return false;
      }
    }

    let timestamp = Date.now();
    let stamp = JSON.stringify(
        {
          type: STAMP,
          stamper: stamper.json(),
          stampee: stampee.json(),
          timestamp: timestamp,
        });
    let signature = this.ident.sign(stamp);
    stamp[SIGNATURE] = signature;
    this.ledger.append(stamp);

    this.generateMembership();

    return true;
  }
}
