let app = undefined;

const clearCreateIdentInputs = function () {
  app.createIdentName = '';
  app.createIdentPass = '';
};

const createIdent = function () {
  fetchData(
      'create-ident',
      {
        'name': app.createIdentName,
        'passphrase': app.createIdentPass,
      }).then(
          (resp) => {
            if (!resp.success) {
              return;
            }

            fetchIdents().then(toggleCreateIdent).then(
                () => {
                  app.currentIdent = app.idents.findIndex(
                      id => id.name == app.createIdentName);
                }).then(clearCreateIdentInputs);
          });
};

const fetchData = function (method, data) {
  return fetch(
      `/rpc/${method}`,
      {
        dataType: 'json',
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
      }).then(r => r.json());
};

const fetchIdents = function () {
  return fetchData('all-idents', {}).then(
      r => r.success ? r.idents : []).then(populateIdents);
};

const populateIdents = function (newIdents) {
  app.idents = [];
  newIdents.forEach(
      (ident, index) => {
        app.idents.push({ 'name': ident.name });
      });
};

const toggleCreateIdent = function () {
  // TODO is there a vue way to do this?
  let formElement = $('div#create-ident');
  let buttonElement = $('button#toggle-create-ident');

  if (formElement.is(':visible')) {
    formElement.hide();
    buttonElement.text('+');
  } else {
    formElement.show();
    buttonElement.text('-');
  }
};

/* ENTRY POINT */

const go = () => {
  fetchIdents();

  app = new Vue({
    el: '#app',
    data: {
      currentIdent: '',
      idents: [],
      createIdentName: '',
      createIdentPass: '',
    },
    methods: {
      createIdent: createIdent,
      toggleCreateIdent: toggleCreateIdent,
    }
  });
};
